import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


    /*
    https://kafka.apache.org/quickstart#
    https://dzone.com/articles/running-apache-kafka-on-windows-os

    cd /d D:\Programming\apache-zookeeper-3.6.2
    .\bin\zkServer.cmd

    cd /d D:\Programming\kafka_2.13-2.7.0
    .\bin\windows\kafka-server-start.bat .\config\server.properties
     */


public class Main {
    // Declaration
    static final String KAFKA_CONSOLE_PRODUCER_PATH = "D:\\Programming\\kafka_2.13-2.7.0\\bin\\windows\\kafka-console-producer.bat";
    static final String INPUT_FILEPATH              = "Datasets\\mushrooms.csv";
    static final int TIME_WINDOW                    = 15;
    static final int BATCH_SIZE                     = 5;
    static final String KAFKA_TOPIC                 = "sparkstreaming";
    static final String KAFKA_SERVER                = "localhost:9092";


    public static void main(String[] args) {
        // Declaration
        List<String> lines;
        Path filePath       = Paths.get(INPUT_FILEPATH);
        Charset charset     = StandardCharsets.UTF_8;
        int curLine         = 0;

        // Read File
        try {
            lines = Files.readAllLines(filePath, charset);
        } catch (IOException ex) {
            System.out.println("I/O Exception: " + ex);
            return;
        }

        // Send Data until File empty
        while (curLine <= lines.size()) {
            // Intern Declaration
            int maxLine;
            List<String> outputLines = new ArrayList<>();

            // Simulate different Window & Batch size
            int timeWindow  = new Random().nextInt(TIME_WINDOW) * 1000;
            int batchSize   = new Random().nextInt(BATCH_SIZE) + 1;

            // Get Max Line for Batch size
            maxLine = Math.min(lines.size(), curLine + batchSize);

            // Simulate Window Waiting
            try {
                System.out.println("Waiting " + timeWindow / 1000 + " seconds.");
                Thread.sleep(timeWindow);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // Write Batch Size
            for (int i = curLine; i < maxLine; i++) {
                outputLines.add(lines.get(i));
            }

            // Set current Line
            curLine = maxLine;

            // Generate File to send
            try {
                File file               = new File("output.csv");
                FileWriter filewriter   = new FileWriter(file,false);

                // Write Output to File
                for(String line: outputLines){
                    filewriter.write(line + "\n");
                }
                filewriter.close();

                // Generate send Command for CMD & send Datafile
                String[] commandArray = {KAFKA_CONSOLE_PRODUCER_PATH, "--broker-list", KAFKA_SERVER,
                        "--topic", KAFKA_TOPIC, "<", file.getName()};
                Runtime.getRuntime().exec(commandArray);

                System.out.println("\t" + batchSize + " lines written.\n");

            } catch (IOException e) {
                System.out.println("\t...not successful");
            }
        }
    }
}
