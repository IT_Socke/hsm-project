package SparkML;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.classification.*;
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.tuning.CrossValidator;
import org.apache.spark.ml.tuning.CrossValidatorModel;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.util.LongAccumulator;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class AlgorithmOptimizer implements Serializable {
    // Global Variable
    java.util.logging.Logger logger;
    SQLContext sqc;
    String datasetName;
    PipelineModel testModel;


    public AlgorithmOptimizer(java.util.logging.Logger logger, String datasetName){
        // Init
        this.logger         = logger;
        this.datasetName    = datasetName;

        // Try to Create Spark Context & Set Spark Logger
        try {
            SparkSession spark = SparkSession
                    .builder()
                    .appName("AlgorithmOptimizer")
                    .master("local[*]")
                    .config("spark.executor.memory", "2g")
                    .getOrCreate();
            JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());
            sqc = new SQLContext(jsc);

            Logger.getRootLogger().setLevel(Level.ERROR);
            Logger.getLogger("org").setLevel(Level.ERROR);
            Logger.getLogger("akka").setLevel(Level.ERROR);
            jsc.sc().setLogLevel("ERROR");
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }
    }


    public AlgorithmOptimizer(String datasetName){
        // Init
        this.datasetName    = datasetName;

        // Try to Create Spark Context & Set Spark Logger
        try {
            SparkSession spark = SparkSession
                    .builder()
                    .appName("AlgorithmOptimizer")
                    .master("local[*]")
                    .config("spark.executor.memory", "2g")
                    .getOrCreate();
            JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());
            sqc = new SQLContext(jsc);
        }
        catch (Exception ignored) { }
    }


    public void loadTestAlgorithm(){
        // Declaration
        File currentDirFile = new File("JavaStream\\ml_models");
        String model_path         = currentDirFile.getAbsolutePath();

        // Load GBT for Tess
        testModel = PipelineModel.load(model_path + "\\gbt_" + datasetName + ".model");
    }


    public void predict(Dataset<Row> dataframe){
        // Check if Test Model loaded
        if(testModel == null)
            return;

        // Make prediction
        Dataset<Row> predictions = testModel.transform(dataframe);

        // Show Predictions
        predictions.show((int) predictions.count());
    }


    public void fit(Dataset<Row> dataframe){
        // Declaration
        File currentDirFile = new File("JavaStream\\ml_models");
        String model_path         = currentDirFile.getAbsolutePath();

        // Check if Dataframe have indexedLabel & indexedFeatures only
        String[] cols = dataframe.columns();
        if(cols.length > 2)
            return;
        else{
            for (String col: cols) {
                if(!col.equals("indexedLabel") && !col.equals("indexedFeatures"))
                    return;
            }
        }

        // Select (prediction, true label) and compute test error
        MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator()
                .setLabelCol("indexedLabel")
                .setPredictionCol("prediction")
                .setMetricName("accuracy");

        //region Logistic Regression
        // Model - Logistic Regression
        LogisticRegression lr = new LogisticRegression()
                .setFeaturesCol("indexedFeatures")
                .setLabelCol("indexedLabel")
                .setPredictionCol("prediction");

        // Create Pipeline
        Pipeline pipeline = new Pipeline()
                .setStages(new PipelineStage[] {lr});

        // Fit/Train the CrossValidator
        ParamMap[] paramGrid = new ParamGridBuilder()
                .addGrid(lr.maxIter(), new int[]{10, 50, 100, 200})
                .addGrid(lr.regParam(), new double[]{0.0, 0.01, 0.1, 0.5, 1.0})
                .addGrid(lr.elasticNetParam(), new double[]{0.0, 0.4, 0.8, 1.0})
                .addGrid(lr.tol(), new double[]{0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1})
                .build();

        // Create CrossValidator
        CrossValidator cv = new CrossValidator()
                .setEstimator(pipeline)
                .setEvaluator(evaluator)
                .setEstimatorParamMaps(paramGrid)
                .setNumFolds(2)
                .setParallelism(2);

        CrossValidatorModel cvModel = cv.fit(dataframe);

        // Get the Model with the best parameters & save it
        PipelineModel bstModel = (PipelineModel) cvModel.bestModel();
        try {
            bstModel.write().overwrite().save(model_path + "\\lr_" + datasetName + ".model");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //endregion

        //region Linear Support Vector Machine
        // Model - Linear Support Vector Machine
        LinearSVC lsvc = new LinearSVC()
                .setFeaturesCol("indexedFeatures")
                .setLabelCol("indexedLabel")
                .setPredictionCol("prediction");

        // Create Pipeline
        pipeline = new Pipeline()
                .setStages(new PipelineStage[] {lsvc});

        // Fit/Train the CrossValidator
        paramGrid = new ParamGridBuilder()
                .addGrid(lsvc.maxIter(), new int[]{50, 100, 200, 1000, 2000})
                .addGrid(lsvc.regParam(), new double[]{0.0, 0.01, 0.1, 0.5, 1.0})
                .addGrid(lsvc.tol(), new double[]{0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1})
                .build();

        // Create CrossValidator
        cv = new CrossValidator()
                .setEstimator(pipeline)
                .setEvaluator(evaluator)
                .setEstimatorParamMaps(paramGrid)
                .setNumFolds(2)
                .setParallelism(2);

        cvModel = cv.fit(dataframe);

        // Get the Model with the best parameters & save it
        bstModel = (PipelineModel) cvModel.bestModel();
        try {
            bstModel.write().overwrite().save(model_path + "\\lsvc_" + datasetName + ".model");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //endregion

        //region Random Forest
        // Model - Random Forest
        RandomForestClassifier rf = new RandomForestClassifier()
                .setFeaturesCol("indexedFeatures")
                .setLabelCol("indexedLabel")
                .setPredictionCol("prediction");

        // Create Pipeline
        pipeline = new Pipeline()
                .setStages(new PipelineStage[] {rf});

        // Fit/Train the CrossValidator
        paramGrid = new ParamGridBuilder()
                .addGrid(rf.numTrees(), new int[]{10, 20, 50, 100, 250})
                .addGrid(rf.maxDepth(), new int[]{5, 10, 15, 25})
                .addGrid(rf.maxBins(), new int[]{16, 32, 64})
                .build();

        // Create CrossValidator
        cv = new CrossValidator()
                .setEstimator(pipeline)
                .setEvaluator(evaluator)
                .setEstimatorParamMaps(paramGrid)
                .setNumFolds(2)
                .setParallelism(2);

        cvModel = cv.fit(dataframe);

        // Get the Model with the best parameters & save it
        bstModel = (PipelineModel) cvModel.bestModel();
        try {
            bstModel.write().overwrite().save(model_path + "\\rf_" + datasetName + ".model");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //endregion

        //region Gradient-boosted tree
        // Model - Random Forest
        GBTClassifier gbt = new GBTClassifier()
                .setFeaturesCol("indexedFeatures")
                .setLabelCol("indexedLabel")
                .setPredictionCol("prediction");

        // Create Pipeline
        pipeline = new Pipeline()
                .setStages(new PipelineStage[] {gbt});

        // Fit/Train the CrossValidator
        paramGrid = new ParamGridBuilder()
                .addGrid(gbt.maxDepth(), new int[]{3, 5, 10, 25})
                .addGrid(gbt.maxBins(), new int[]{32, 64, 128, 256})
                .addGrid(gbt.maxIter(), new int[]{10, 20, 50, 100})
                .build();

        // Create CrossValidator
        cv = new CrossValidator()
                .setEstimator(pipeline)
                .setEvaluator(evaluator)
                .setEstimatorParamMaps(paramGrid)
                .setNumFolds(2)
                .setParallelism(2);

        cvModel = cv.fit(dataframe);

        // Get the Model with the best parameters & save it
        bstModel = (PipelineModel) cvModel.bestModel();
        try {
            bstModel.write().overwrite().save(model_path + "\\gbt_" + datasetName + ".model");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //endregion
    }


    public void validate(Dataset<Row> dataframe){
        // Declaration
        double bestAccuracy = 0.0;
        PipelineModel bestModel = null;
        String algoName     = "";
        // Declaration
        File currentDirFile = new File("JavaStream\\ml_models");
        String model_path         = currentDirFile.getAbsolutePath();
        String[] algoNames  = {"Logistic Regression", "Linear Support Vector Machine",
                "Random Forest", "Gradient-boosted tree"};

        // Check if Dataframe have indexedLabel & indexedFeatures only
        String[] cols = dataframe.columns();
        if(cols.length > 2)
            return;
        else{
            for (String col: cols) {
                if(!col.equals("indexedLabel") && !col.equals("indexedFeatures"))
                    return;
            }
        }

        // Load Models
        try {
            PipelineModel[] models = {
                    PipelineModel.load(model_path + "\\lr_" + datasetName + ".model"),
                    PipelineModel.load(model_path + "\\lsvc_" + datasetName + ".model"),
                    PipelineModel.load(model_path + "\\rf_" + datasetName + ".model"),
                    PipelineModel.load(model_path + "\\gbt_" + datasetName + ".model")
            };

            for (int i = 0; i < models.length; i++) {
                // Make predictions
                Dataset<Row> predictions = models[i].transform(dataframe);

                // Select (prediction, true label) and compute test error
                MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator()
                        .setLabelCol("indexedLabel")
                        .setPredictionCol("prediction")
                        .setMetricName("accuracy");

                // Print Score
                double currAccuracy = (Math.round(evaluator.evaluate(predictions) * 10000.0) / 100.0);
                System.out.printf("%s: %f\n", algoNames[i], currAccuracy);

                // Set best Accuracy
                if(currAccuracy > bestAccuracy){
                    bestAccuracy = currAccuracy;
                    bestModel = models[i];
                    algoName = algoNames[i];
                }
            }

            System.out.println("\nBest Algorithm: " + algoName);
            System.out.println("Accuracy: " + bestAccuracy);
            logger.log(java.util.logging.Level.INFO, "Best Algorithm: " + algoName);
            logger.log(java.util.logging.Level.INFO, "Accuracy: " + bestAccuracy + "%");
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }

        // Print Result Matrix
        if(bestModel != null)
            printResultMatrix(dataframe, bestModel);
    }


    public void printResultMatrix(Dataset<Row> dfTest, PipelineModel model){
        // Declaration
        LongAccumulator tp = sqc.sparkContext().longAccumulator();
        LongAccumulator fp = sqc.sparkContext().longAccumulator();
        LongAccumulator tn = sqc.sparkContext().longAccumulator();
        LongAccumulator fn = sqc.sparkContext().longAccumulator();

        // Make predictions
        Dataset<Row> predictions = model.transform(dfTest);
        predictions.foreach((ForeachFunction<Row>) row -> {
            double label = 0;
            double prediction = 0;
            try {
                label = row.getAs("indexedLabel");
                prediction = row.getAs("prediction");
            }
            catch (Exception ex){
                System.out.println("Something is wrong, with the Result Matrix Cast.");
            }

            switch((int)label){
                case 0:
                    if(label == prediction)
                        tn.add(1);
                    else
                        fp.add(1);
                    break;
                case 1:
                    if(label == prediction)
                        tp.add(1);
                    else
                        fn.add(1);
                    break;
                default:
                    System.out.println("Something is wrong, with the Result Matrix.");
            }
        });

        System.out.printf("TP: %d, FP: %d, TN: %d, FN: %d\n", tp.count(), fp.count(), tn.count(),
                fn.count());
    }
}
