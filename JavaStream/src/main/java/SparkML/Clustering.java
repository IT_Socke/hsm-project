package SparkML;

import org.apache.commons.math3.ml.clustering.evaluation.ClusterEvaluator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.clustering.GaussianMixture;
import org.apache.spark.ml.clustering.GaussianMixtureModel;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.evaluation.ClusteringEvaluator;
import org.apache.spark.ml.param.Param;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

public class Clustering {
    // Global Variable
    java.util.logging.Logger logger;
    SQLContext sqc;
    String datasetName;


    public Clustering(java.util.logging.Logger logger, String datasetName){
        // Init
        this.logger         = logger;
        this.datasetName    = datasetName;

        // Try to Create Spark Context & Set Spark Logger
        try {
            SparkSession spark = SparkSession
                    .builder()
                    .appName("Clustering")
                    .master("local[*]")
                    .config("spark.executor.memory", "2g")
                    .getOrCreate();
            JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());
            sqc = new SQLContext(jsc);

            Logger.getRootLogger().setLevel(Level.ERROR);
            Logger.getLogger("org").setLevel(Level.ERROR);
            Logger.getLogger("akka").setLevel(Level.ERROR);
            jsc.sc().setLogLevel("ERROR");
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }
    }


    public int optimizeKforKMeans(Dataset<Row> dataframe){
        // Check if Dataframe have indexedLabel & indexedFeatures only
        String[] cols = dataframe.columns();
        if(cols.length > 1)
            return 1;
        else{
            for (String col: cols) {
                if(!col.equals("indexedFeatures"))
                    return 1;
            }
        }

        // Declaration
        double bestSilhouetteScoreKMeans    = -1;
        int bestK = 1;

        // Find optimal k param
        for (int k = 2; k < 21; k++) {
            // Trains a kMeans
            double currSilKMeans = silhouetteScoreKMeans(dataframe, k);
            if(currSilKMeans > bestSilhouetteScoreKMeans) {
                bestSilhouetteScoreKMeans = currSilKMeans;
                bestK = k;
            }
        }

        System.out.printf("Best Silhouette Score for kMeans is: %f\n", bestSilhouetteScoreKMeans);
        System.out.printf("Best k Param for kMeans is: %d\n", bestK);

        return bestK;
    }


    public int optimizeKforGMM(Dataset<Row> dataframe){
        // Check if Dataframe have indexedLabel & indexedFeatures only
        String[] cols = dataframe.columns();
        if(cols.length > 1)
            return 1;
        else{
            for (String col: cols) {
                if(!col.equals("indexedFeatures"))
                    return 1;
            }
        }

        // Declaration
        double bestSilhouetteScoreGMM = -1;
        int bestK = 1;

        // Find optimal k param
        for (int k = 2; k < 21; k++) {
            // Train a GaussianMixture Model
            double currSilGMM = silhouetteScoreGMM(dataframe, k);
            if(currSilGMM > bestSilhouetteScoreGMM) {
                bestSilhouetteScoreGMM = currSilGMM;
                bestK = k;
            }
        }

        System.out.printf("Best Silhouette Score for GMM is: %f\n", bestSilhouetteScoreGMM);
        System.out.printf("Best k Param for GMM is: %d\n", bestK);

        return bestK;
    }


    public double silhouetteScoreKMeans(Dataset<Row> dataframe, int k){
        // Check if Dataframe have indexedLabel & indexedFeatures only
        String[] cols = dataframe.columns();
        if(cols.length > 1)
            return -1;
        else{
            for (String col: cols) {
                if(!col.equals("indexedFeatures"))
                    return -1;
            }
        }

        /// Trains a k-means model.
        KMeans kmeans = new KMeans()
                .setK(k)
                .setMaxIter(100)
                .setFeaturesCol("indexedFeatures");
        KMeansModel modelKMeans = kmeans.fit(dataframe);

        // Evaluator
        ClusteringEvaluator evaluator = new ClusteringEvaluator()
                .setFeaturesCol("indexedFeatures");

        // Make predictions
        Dataset<Row> predictions = modelKMeans.transform(dataframe);

        return evaluator.evaluate(predictions);
    }


    public double silhouetteScoreGMM(Dataset<Row> dataframe, int k){
        // Check if Dataframe have indexedLabel & indexedFeatures only
        String[] cols = dataframe.columns();
        if(cols.length > 1)
            return -1;
        else{
            for (String col: cols) {
                if(!col.equals("indexedFeatures"))
                    return -1;
            }
        }

        // Train a GaussianMixture model
        GaussianMixture gmm = new GaussianMixture()
                .setK(k)
                .setFeaturesCol("indexedFeatures");
        GaussianMixtureModel gmmModel = gmm.fit(dataframe);

        // Evaluator
        ClusteringEvaluator evaluator = new ClusteringEvaluator()
                .setFeaturesCol("indexedFeatures");

        // Make predictions
        Dataset<Row> predictions = gmmModel.transform(dataframe);

        return evaluator.evaluate(predictions);
    }
}
