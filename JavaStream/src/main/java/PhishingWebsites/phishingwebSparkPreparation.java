package PhishingWebsites;

import Books.bookObj;
import Books.bookStream;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.feature.VectorIndexer;
import org.apache.spark.ml.feature.VectorIndexerModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.List;
import java.util.regex.Pattern;

public class phishingwebSparkPreparation {
    // Global Variable
    java.util.logging.Logger logger;
    transient SparkSession spark;
    transient JavaSparkContext jsc;


    public phishingwebSparkPreparation(java.util.logging.Logger logger){
        // Init
        this.logger = logger;

        // Try to Create Spark Context & Set Spark Logger
        try {
            SparkConf conf = new SparkConf()
                    .setAppName("SparkPhishingwebPreparation")
                    .setMaster("local[*]")
                    .set("spark.executor.memory", "16g")
                    .setSparkHome(System.getenv("SPARK_HOME"));

            spark = SparkSession
                    .builder()
                    .config(conf)
                    .getOrCreate();
            jsc = new JavaSparkContext(spark.sparkContext());

            Logger.getRootLogger().setLevel(Level.ERROR);
            Logger.getLogger("org").setLevel(Level.ERROR);
            Logger.getLogger("akka").setLevel(Level.ERROR);
            spark.sparkContext().setLogLevel("ERROR");
            jsc.sc().setLogLevel("ERROR");
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }
    }


    public Dataset<Row> getData(){
        // Declaration
        String separator = "|";

        // Read CSV
        JavaRDD<String> data = jsc.textFile("Datasets/phishing_websites.csv");

        // Cast String RDD to specific Object RDD
        JavaRDD<phishingwebObj> rrdData = data.map(line -> {
            line = line.replace(",", "|");
            try {
                String[] attributes = line.split(Pattern.quote(separator));
                return new phishingwebObj(attributes);
            } catch (Exception ex) {
                return new phishingwebObj();
            }
        })
                .filter(phishingwebObj -> phishingwebObj.getResult() != -1);

        // Queries
        doQueries(rrdData);

        // Create Dataframe from JavaRDD
        Dataset<Row> dfBooks = spark.createDataFrame(rrdData, phishingwebObj.class);

        // Cast List to SparkDataset
        return castData(dfBooks);
    }


    private void doQueries(JavaRDD<phishingwebObj> phishingStream) {
        // Declaration
        final int[] iCount = {0};

        // Simple Queries
        JavaRDD<bookObj>[] simpleQuery1 = new JavaRDD[]{phishingStream.filter(phishingwebObj -> phishingwebObj.getAbnormal_URL() == -1)};
        System.out.println("Phishing Entries where an Abnormal_URL is: " + simpleQuery1[0].count());

        // complex queries
        long complexQuery1= phishingStream
                .map(phishingwebObj -> {
                    if(phishingwebObj.getResult() == 1)
                        iCount[0] += 1;
                    else if (phishingwebObj.getResult() == -1)
                        iCount[0] -= 1;
                    return phishingwebObj;
                })
                .filter(phishingwebObj -> phishingwebObj.getResult() == 1 && iCount[0] >= 0 & phishingwebObj.getHaving_At_Symbol() == -1 ||
                        phishingwebObj.getResult() == -1 && iCount[0] < 0 & phishingwebObj.getHaving_At_Symbol() == -1)
                .count();

        if(iCount[0] >= 0)
            System.out.println("Most entries with Class 1 from Phishing Entries are Having_At_Symbol = 1: " + complexQuery1);
        else
            System.out.println("Most entries with Class -1 from Phishing Entries are Having_At_Symbol = -1: " + complexQuery1);
        long complexQuery2= phishingStream
                .map(phishingwebObj -> {
                    if(phishingwebObj.getHaving_IP_Address() == 1)
                        iCount[0] += 1;
                    else if (phishingwebObj.getHaving_IP_Address() == -1)
                        iCount[0] -= 1;
                    return phishingwebObj;
                })
                .filter(phishingwebObj -> phishingwebObj.getHaving_IP_Address() == 1 && iCount[0] >= 0 & phishingwebObj.getPrefix_Suffix() == -1 ||
                        phishingwebObj.getHaving_IP_Address() == -1 && iCount[0] < 0 & phishingwebObj.getPrefix_Suffix() == -1)
                .count();

        if(iCount[0] >= 0)
            System.out.println("Most entries with Having_IP_Address = 1 from Phishing Entries are Prefix_Suffix = 1: " + complexQuery2);
        else
            System.out.println("Most entries with Having_IP_Address = -1 from Phishing Entries are Prefix_Suffix = -1: " + complexQuery2);
    }


    public Dataset<Row>[] trainTestSplit(Dataset<Row> dfMushrooms){
        // Prepare training and test data.
        return dfMushrooms.randomSplit(new double[] {0.8, 0.2},0);
    }


    private Dataset<Row> castData(List<phishingwebObj> phishingwebList){
        Dataset<Row> dfPhishingWeb = spark.createDataFrame(phishingwebList, phishingwebObj.class);

        return castData(dfPhishingWeb);
    }


    private Dataset<Row> castData(Dataset<Row> dfPhishingWeb){
        dfPhishingWeb = dfPhishingWeb.withColumnRenamed("Result", "indexedLabel");
        String[] columns = dfPhishingWeb.columns();

        // DataFrames formatter
        VectorAssembler assembler = new VectorAssembler()
                .setInputCols(columns)
                .setOutputCol("features");
        dfPhishingWeb = assembler.transform(dfPhishingWeb);

        // Indexer for features
        VectorIndexer featureIndexer = new VectorIndexer()
                .setInputCol("features")
                .setOutputCol("indexedFeatures");
        VectorIndexerModel indexerModelFeature = featureIndexer.fit(dfPhishingWeb);
        dfPhishingWeb = indexerModelFeature.transform(dfPhishingWeb);


        return dfPhishingWeb.selectExpr("cast(indexedLabel as double) indexedLabel", "indexedFeatures");
    }
}
