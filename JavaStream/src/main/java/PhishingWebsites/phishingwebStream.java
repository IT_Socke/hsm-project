package PhishingWebsites;


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class phishingwebStream {
    Logger logger;

    public phishingwebStream(Logger logger){
        this.logger = logger;
    }

    public void extract(){
        // Deklaration
        BufferedReader br;
        String separator = "|";

        // Try to read file
        try {
            br = new BufferedReader(new FileReader("Datasets/phishing_websites.csv"));
        }
        catch (Exception ignore) {
            logger.log(Level.WARNING, "File not found");
            return;
        }

        // Convert Stream to Obj List
        Stream<phishingwebObj> phishingStream = br.lines()
                .skip(1)
                .map(line -> {
                    line = line.replace(",", "|");
                    try {
                        String[] attributes = line.split(Pattern.quote(separator));
                        return new phishingwebObj(attributes);
                    } catch (Exception ex){
                        logger.log(Level.WARNING, ex.toString());
                        return null;
                    }
                })
                .filter(Objects::nonNull);

        doQueries(phishingStream);
    }

    private void doQueries(Stream<phishingwebObj> phishingStream) {
        // Declaration
        var ref = new Object() {
            int iCount = 0;
        };

        // Simple Queries
        Stream<phishingwebObj> simpleQuerie1 = phishingStream.filter(phishingwebObj -> phishingwebObj.getResult() == 1);
        System.out.println("Phishing Entries where Class = 1: " + simpleQuerie1.count());

        // complex queries
        /*Stream<phishingwebObj> complexQuery1 = phishingStream
                .sorted()
                .filter(phishingwebObj -> phishingwebObj.getResult() == 1);
        System.out.println("Sorted Phishing Entries from Class 1: " + complexQuery1);
        long complexQuery2 = phishingStream
                .peek(phishingwebObj -> {
                    if(phishingwebObj.getResult() == 1)
                        ref.iCount += 1;
                    else if (phishingwebObj.getResult() == -1)
                        ref.iCount -= 1;
                })
                .filter(phishingwebObj -> phishingwebObj.getResult() == 1 && ref.iCount >= 0 & phishingwebObj.getHaving_At_Symbol() == -1 ||
                        phishingwebObj.getResult() == -1 && ref.iCount < 0 & phishingwebObj.getHaving_At_Symbol() == -1)
                .count();

        if(ref.iCount >= 0)
            System.out.println("Most entries are Phishing Entries from Class 1 with Having_At_Symbol = 1: " + complexQuery2);
        else
            System.out.println("Most entries are Phishing Entries from Class -1 with Having_At_Symbol = -1: " + complexQuery2);*/
    }
}
