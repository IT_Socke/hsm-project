package PhishingWebsites;

public class phishingwebObj implements Comparable{
    private int having_IP_Address           = -1;
    private int URL_Length                  = -1;
    private int Shortining_Service          = -1;
    private int having_At_Symbol            = -1;
    private int double_slash_redirecting    = -1;
    private int Prefix_Suffix               = -1;
    private int having_Sub_Domain           = -1;
    private int SSLfinal_State              = -1;
    private int Domain_registeration_length = -1;
    private int Favicon                     = -1;
    private int port                        = -1;
    private int HTTPS_token                 = -1;
    private int Request_URL                 = -1;
    private int URL_of_Anchor               = -1;
    private int Links_in_tags               = -1;
    private int SFH                         = -1;
    private int Submitting_to_email         = -1;
    private int Abnormal_URL                = -1;
    private int Redirect                    = -1;
    private int on_mouseover                = -1;
    private int RightClick                  = -1;
    private int popUpWidnow                 = -1;
    private int Iframe                      = -1;
    private int age_of_domain               = -1;
    private int DNSRecord                   = -1;
    private int web_traffic                 = -1;
    private int Page_Rank                   = -1;
    private int Google_Index                = -1;
    private int Links_pointing_to_page      = -1;
    private int Statistical_report          = -1;
    private int Result                      = -1;

    public phishingwebObj(){}

    public phishingwebObj(String[] s) {
        super();
        setHaving_IP_Address(Integer.parseInt(s[0]));
        setURL_Length(Integer.parseInt(s[1]));
        setShortining_Service(Integer.parseInt(s[2]));
        setHaving_At_Symbol(Integer.parseInt(s[3]));
        setDouble_slash_redirecting(Integer.parseInt(s[4]));
        setPrefix_Suffix(Integer.parseInt(s[5]));
        setHaving_Sub_Domain(Integer.parseInt(s[6]));
        setSSLfinal_State(Integer.parseInt(s[7]));
        setDomain_registeration_length(Integer.parseInt(s[8]));
        setFavicon(Integer.parseInt(s[9]));
        setPort(Integer.parseInt(s[10]));
        setHTTPS_token(Integer.parseInt(s[11]));
        setRequest_URL(Integer.parseInt(s[12]));
        setURL_of_Anchor(Integer.parseInt(s[13]));
        setLinks_in_tags(Integer.parseInt(s[14]));
        setSFH(Integer.parseInt(s[15]));
        setSubmitting_to_email(Integer.parseInt(s[16]));
        setAbnormal_URL(Integer.parseInt(s[17]));
        setRedirect(Integer.parseInt(s[18]));
        setOn_mouseover(Integer.parseInt(s[19]));
        setRightClick(Integer.parseInt(s[20]));
        setPopUpWidnow(Integer.parseInt(s[21]));
        setIframe(Integer.parseInt(s[22]));
        setAge_of_domain(Integer.parseInt(s[23]));
        setDNSRecord(Integer.parseInt(s[24]));
        setWeb_traffic(Integer.parseInt(s[25]));
        setPage_Rank(Integer.parseInt(s[26]));
        setGoogle_Index(Integer.parseInt(s[27]));
        setLinks_pointing_to_page(Integer.parseInt(s[28]));
        setStatistical_report(Integer.parseInt(s[29]));
        setResult(Integer.parseInt(s[30]));
    }



    public int getHaving_IP_Address() {
        return having_IP_Address;
    }

    public void setHaving_IP_Address(int having_IP_Address) {
        this.having_IP_Address = having_IP_Address;
    }

    public int getURL_Length() {
        return URL_Length;
    }

    public void setURL_Length(int URL_Length) {
        this.URL_Length = URL_Length;
    }

    public int getShortining_Service() {
        return Shortining_Service;
    }

    public void setShortining_Service(int shortining_Service) {
        Shortining_Service = shortining_Service;
    }

    public int getHaving_At_Symbol() {
        return having_At_Symbol;
    }

    public void setHaving_At_Symbol(int having_At_Symbol) {
        this.having_At_Symbol = having_At_Symbol;
    }

    public int getDouble_slash_redirecting() {
        return double_slash_redirecting;
    }

    public void setDouble_slash_redirecting(int double_slash_redirecting) {
        this.double_slash_redirecting = double_slash_redirecting;
    }

    public int getPrefix_Suffix() {
        return Prefix_Suffix;
    }

    public void setPrefix_Suffix(int prefix_Suffix) {
        Prefix_Suffix = prefix_Suffix;
    }

    public int getHaving_Sub_Domain() {
        return having_Sub_Domain;
    }

    public void setHaving_Sub_Domain(int having_Sub_Domain) {
        this.having_Sub_Domain = having_Sub_Domain;
    }

    public int getSSLfinal_State() {
        return SSLfinal_State;
    }

    public void setSSLfinal_State(int SSLfinal_State) {
        this.SSLfinal_State = SSLfinal_State;
    }

    public int getDomain_registeration_length() {
        return Domain_registeration_length;
    }

    public void setDomain_registeration_length(int domain_registeration_length) {
        Domain_registeration_length = domain_registeration_length;
    }

    public int getFavicon() {
        return Favicon;
    }

    public void setFavicon(int favicon) {
        Favicon = favicon;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getHTTPS_token() {
        return HTTPS_token;
    }

    public void setHTTPS_token(int HTTPS_token) {
        this.HTTPS_token = HTTPS_token;
    }

    public int getRequest_URL() {
        return Request_URL;
    }

    public void setRequest_URL(int request_URL) {
        Request_URL = request_URL;
    }

    public int getURL_of_Anchor() {
        return URL_of_Anchor;
    }

    public void setURL_of_Anchor(int URL_of_Anchor) {
        this.URL_of_Anchor = URL_of_Anchor;
    }

    public int getLinks_in_tags() {
        return Links_in_tags;
    }

    public void setLinks_in_tags(int links_in_tags) {
        Links_in_tags = links_in_tags;
    }

    public int getSFH() {
        return SFH;
    }

    public void setSFH(int SFH) {
        this.SFH = SFH;
    }

    public int getSubmitting_to_email() {
        return Submitting_to_email;
    }

    public void setSubmitting_to_email(int submitting_to_email) {
        Submitting_to_email = submitting_to_email;
    }

    public int getAbnormal_URL() {
        return Abnormal_URL;
    }

    public void setAbnormal_URL(int abnormal_URL) {
        Abnormal_URL = abnormal_URL;
    }

    public int getRedirect() {
        return Redirect;
    }

    public void setRedirect(int redirect) {
        Redirect = redirect;
    }

    public int getOn_mouseover() {
        return on_mouseover;
    }

    public void setOn_mouseover(int on_mouseover) {
        this.on_mouseover = on_mouseover;
    }

    public int getRightClick() {
        return RightClick;
    }

    public void setRightClick(int rightClick) {
        RightClick = rightClick;
    }

    public int getPopUpWidnow() {
        return popUpWidnow;
    }

    public void setPopUpWidnow(int popUpWidnow) {
        this.popUpWidnow = popUpWidnow;
    }

    public int getIframe() {
        return Iframe;
    }

    public void setIframe(int iframe) {
        Iframe = iframe;
    }

    public int getAge_of_domain() {
        return age_of_domain;
    }

    public void setAge_of_domain(int age_of_domain) {
        this.age_of_domain = age_of_domain;
    }

    public int getDNSRecord() {
        return DNSRecord;
    }

    public void setDNSRecord(int DNSRecord) {
        this.DNSRecord = DNSRecord;
    }

    public int getWeb_traffic() {
        return web_traffic;
    }

    public void setWeb_traffic(int web_traffic) {
        this.web_traffic = web_traffic;
    }

    public int getPage_Rank() {
        return Page_Rank;
    }

    public void setPage_Rank(int page_Rank) {
        Page_Rank = page_Rank;
    }

    public int getGoogle_Index() {
        return Google_Index;
    }

    public void setGoogle_Index(int google_Index) {
        Google_Index = google_Index;
    }

    public int getLinks_pointing_to_page() {
        return Links_pointing_to_page;
    }

    public void setLinks_pointing_to_page(int links_pointing_to_page) {
        Links_pointing_to_page = links_pointing_to_page;
    }

    public int getStatistical_report() {
        return Statistical_report;
    }

    public void setStatistical_report(int statistical_report) {
        Statistical_report = statistical_report;
    }

    public int getResult() {
        return Result;
    }

    public void setResult(int result) {
        if(result == -1)
            Result = 0;
        else
            Result = result;
    }

    @Override
    public int compareTo(Object o) {
        phishingwebObj otherObj = (phishingwebObj) o;
        return Integer.compare(this.getHaving_IP_Address(), otherObj.getHaving_IP_Address());
    }
}
