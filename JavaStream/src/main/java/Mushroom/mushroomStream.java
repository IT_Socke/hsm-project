package Mushroom;

import PhishingWebsites.phishingwebObj;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class mushroomStream {
    Logger logger;

    public mushroomStream(Logger logger){
        this.logger = logger;
    }

    public void extract(){
        // Deklaration
        BufferedReader br;
        String separator = "|";

        // Try to read file
        try {
            br = new BufferedReader(new FileReader("Datasets/mushrooms.csv"));
        }
        catch (Exception ignore) {
            logger.log(Level.WARNING, "File not found");
            return ;
        }

        // Convert Stream to Obj List
        Stream<mushroomObj> mushroomStream = br.lines()
                .skip(1)
                .map(line -> {
                    line = line.replace(",", "|");
                    try {
                        String[] attributes = line.split(Pattern.quote(separator));
                        return new mushroomObj(attributes);
                    } catch (Exception ex){
                        logger.log(Level.WARNING, ex.toString());
                        return null;
                    }
                })
                .filter(Objects::nonNull);

        doQueries(mushroomStream);
    }


    private void doQueries(Stream<mushroomObj> mushroomStream) {
        // Declaration
        var ref = new Object() {
            int iCount = 0;
        };

        // Simple Queries
        //Stream<mushroomObj> simpleQuerie1 = mushroomStream.filter(mushroomObj -> mushroomObj.getcClass().equals("e"));
        //System.out.println("Mushroom Entries where eatable: " + simpleQuerie1.count());

        // complex queries
        /*long complexQuerie1 = mushroomStream
                .peek(mushroomObj -> {
                    if(mushroomObj.getcClass().equals("e"))
                        ref.iCount += 1;
                    else if (mushroomObj.getcClass().equals("p"))
                        ref.iCount -= 1;
                })
                .filter(mushroomObj -> mushroomObj.getcClass().equals("e") && ref.iCount >= 0 & mushroomObj.getCap_shape().equals("x") ||
                        mushroomObj.getcClass().equals("p") && ref.iCount < 0 & mushroomObj.getCap_shape().equals("x"))
                .count();

        if(ref.iCount >= 0)
            System.out.println("eatable mushrooms with cap shape = x: " + complexQuerie1);
        else
            System.out.println("toxic mushrooms with cap shape = x: " + complexQuerie1);*/
        String complexQuerie2 = mushroomStream
                .collect(
                        Collectors.groupingBy(
                                mushroomObj::getCap_color,
                                Collectors.groupingBy(mushroomObj::getcClass)
                        )
                )
                .entrySet().stream().max(Comparator.comparingInt(e -> e.getValue().size()))
                .stream().max(Comparator.comparingInt(e -> e.getValue().size()))
                .map(Map.Entry::getKey).get();
        System.out.println("the class (edible, poisonous) of the majority of mushrooms of the largest group of mushrooms by cap color: " + complexQuerie2);
    }
}
