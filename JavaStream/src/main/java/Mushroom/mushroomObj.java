package Mushroom;

import java.io.Serializable;

public class mushroomObj implements Serializable {
    private char cClass                     = '#';
    private char cap_shape                  = '0';
    private char cap_surface                = '0';
    private char cap_color                  = '0';
    private char bruises                    = '0';
    private char odor                       = '0';
    private char gill_attachment            = '0';
    private char gill_spacing               = '0';
    private char gill_size                  = '0';
    private char gill_color                 = '0';
    private char stalk_shape                = '0';
    private char stalk_root                 = '0';
    private char stalk_surface_above_ring   = '0';
    private char stalk_surface_below_ring   = '0';
    private char stalk_color_above_ring     = '0';
    private char stalk_color_below_ring     = '0';
    private char veil_type                  = '0';
    private char veil_color                 = '0';
    private char ring_number                = '0';
    private char ring_type                  = '0';
    private char spore_print_color          = '0';
    private char population                 = '0';
    private char habitat                    = '0';

    public mushroomObj(){
        super();
    }

    public mushroomObj(String[] s){
        super();
        setcClass(s[0].charAt(0));
        setCap_shape(s[1].charAt(0));
        setCap_surface(s[2].charAt(0));
        setCap_color(s[3].charAt(0));
        setBruises(s[4].charAt(0));
        setOdor(s[5].charAt(0));
        setGill_attachment(s[6].charAt(0));
        setGill_spacing(s[7].charAt(0));
        setGill_size(s[8].charAt(0));
        setGill_color(s[9].charAt(0));
        setStalk_shape(s[10].charAt(0));
        setStalk_root(s[11].charAt(0));
        setStalk_surface_above_ring(s[12].charAt(0));
        setStalk_surface_below_ring(s[13].charAt(0));
        setStalk_color_above_ring(s[14].charAt(0));
        setStalk_color_below_ring(s[15].charAt(0));
        setVeil_type(s[16].charAt(0));
        setVeil_color(s[17].charAt(0));
        setRing_number(s[18].charAt(0));
        setRing_type(s[19].charAt(0));
        setSpore_print_color(s[20].charAt(0));
        setPopulation(s[21].charAt(0));
        setHabitat(s[22].charAt(0));
    }

    @Override
    public String toString() {
        return String.valueOf(getcClass());
    }

    public String getcClass() {
        return String.valueOf(cClass);
    }

    public void setcClass(char cClass) {
        this.cClass = cClass;
    }

    public String getCap_shape() {
        return String.valueOf(cap_shape);
    }

    public void setCap_shape(char cap_shape) {
        this.cap_shape = cap_shape;
    }

    public String getCap_surface() {
        return String.valueOf(cap_surface);
    }

    public void setCap_surface(char cap_surface) {
        this.cap_surface = cap_surface;
    }

    public String getCap_color() {
        return String.valueOf(cap_color);
    }

    public void setCap_color(char cap_color) {
        this.cap_color = cap_color;
    }

    public String getBruises() {
        return String.valueOf(bruises);
    }

    public void setBruises(char bruises) {
        this.bruises = bruises;
    }

    public String getOdor() {
        return String.valueOf(odor);
    }

    public void setOdor(char odor) {
        this.odor = odor;
    }

    public String getGill_attachment() {
        return String.valueOf(gill_attachment);
    }

    public void setGill_attachment(char gill_attachment) {
        this.gill_attachment = gill_attachment;
    }

    public String getGill_spacing() {
        return String.valueOf(gill_spacing);
    }

    public void setGill_spacing(char gill_spacing) {
        this.gill_spacing = gill_spacing;
    }

    public String getGill_size() {
        return String.valueOf(gill_size);
    }

    public void setGill_size(char gill_size) {
        this.gill_size = gill_size;
    }

    public String getGill_color() {
        return String.valueOf(gill_color);
    }

    public void setGill_color(char gill_color) {
        this.gill_color = gill_color;
    }

    public String getStalk_shape() {
        return String.valueOf(stalk_shape);
    }

    public void setStalk_shape(char stalk_shape) {
        this.stalk_shape = stalk_shape;
    }

    public String getStalk_root() {
        return String.valueOf(stalk_root);
    }

    public void setStalk_root(char stalk_root) {
        this.stalk_root = stalk_root;
    }

    public String getStalk_surface_above_ring() {
        return String.valueOf(stalk_surface_above_ring);
    }

    public void setStalk_surface_above_ring(char stalk_surface_above_ring) {
        this.stalk_surface_above_ring = stalk_surface_above_ring;
    }

    public String getStalk_surface_below_ring() {
        return String.valueOf(stalk_surface_below_ring);
    }

    public void setStalk_surface_below_ring(char stalk_surface_below_ring) {
        this.stalk_surface_below_ring = stalk_surface_below_ring;
    }

    public String getStalk_color_above_ring() {
        return String.valueOf(stalk_color_above_ring);
    }

    public void setStalk_color_above_ring(char stalk_color_above_ring) {
        this.stalk_color_above_ring = stalk_color_above_ring;
    }

    public String getStalk_color_below_ring() {
        return String.valueOf(stalk_color_below_ring);
    }

    public void setStalk_color_below_ring(char stalk_color_below_ring) {
        this.stalk_color_below_ring = stalk_color_below_ring;
    }

    public String getVeil_type() {
        return String.valueOf(veil_type);
    }

    public void setVeil_type(char veil_type) {
        this.veil_type = veil_type;
    }

    public String getVeil_color() {
        return String.valueOf(veil_color);
    }

    public void setVeil_color(char veil_color) {
        this.veil_color = veil_color;
    }

    public String getRing_number() {
        return String.valueOf(ring_number);
    }

    public void setRing_number(char ring_number) {
        this.ring_number = ring_number;
    }

    public String getRing_type() {
        return String.valueOf(ring_type);
    }

    public void setRing_type(char ring_type) {
        this.ring_type = ring_type;
    }

    public String getSpore_print_color() {
        return String.valueOf(spore_print_color);
    }

    public void setSpore_print_color(char spore_print_color) {
        this.spore_print_color = spore_print_color;
    }

    public String getPopulation() {
        return String.valueOf(population);
    }

    public void setPopulation(char population) {
        this.population = population;
    }

    public String getHabitat() {
        return String.valueOf(habitat);
    }

    public void setHabitat(char habitat) {
        this.habitat = habitat;
    }
}
