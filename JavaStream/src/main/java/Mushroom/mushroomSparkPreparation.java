package Mushroom;

import Books.bookObj;
import PhishingWebsites.phishingwebObj;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.sql.SparkSession;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class mushroomSparkPreparation implements Serializable {
    // Global Variable
    java.util.logging.Logger logger;
    transient SparkSession spark;
    transient JavaSparkContext jsc;


    public mushroomSparkPreparation(java.util.logging.Logger logger) {
        // Init
        this.logger = logger;

        // Try to Create Spark Context & Set Spark Logger
        try {
            SparkConf conf = new SparkConf()
                    .setAppName("SparkMushroomPreparation")
                    .setMaster("local[*]")
                    .set("spark.executor.memory", "16g")
                    .setSparkHome(System.getenv("SPARK_HOME"));

            spark = SparkSession
                    .builder()
                    .config(conf)
                    .getOrCreate();
            jsc = new JavaSparkContext(spark.sparkContext());

            Logger.getRootLogger().setLevel(Level.ERROR);
            Logger.getLogger("org").setLevel(Level.ERROR);
            Logger.getLogger("akka").setLevel(Level.ERROR);
            spark.sparkContext().setLogLevel("ERROR");
            jsc.sc().setLogLevel("ERROR");
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }
    }


    public mushroomSparkPreparation(){
        // Try to Create Spark Context & Set Spark Logger
        try {
            SparkConf conf = new SparkConf()
                    .setAppName("SparkMushroomPreparation")
                    .setMaster("local[*]")
                    .set("spark.executor.memory", "16g")
                    .setSparkHome(System.getenv("SPARK_HOME"));

            spark = SparkSession
                    .builder()
                    .config(conf)
                    .getOrCreate();
            jsc = new JavaSparkContext(spark.sparkContext());
        }
        catch (Exception ignored) { }
    }


    public Dataset<Row> getData(){
        // Declaration
        String separator = "|";

        // Read CSV
        JavaRDD<String> data = jsc.textFile("Datasets/mushrooms.csv");

        // Cast String RDD to specific Object RDD
        JavaRDD<mushroomObj> rrdData = data.map(line -> {
            line = line.replace(",", "|");
            try {
                String[] attributes = line.split(Pattern.quote(separator));
                return new mushroomObj(attributes);
            } catch (Exception ex) {
                return new mushroomObj();
            }
        })
                .filter(mushroomObj -> !mushroomObj.getcClass().equals("#"));

        // Queries
        doQueries(rrdData);

        // Create Dataframe from JavaRDD
        Dataset<Row> dfBooks = spark.createDataFrame(rrdData, mushroomObj.class);

        // Cast List to SparkDataset
        return castData(dfBooks);
    }


    private void doQueries(JavaRDD<mushroomObj> mushroomStream) {
        // Declaration
        final int[] iCount = {0};

        // Simple Queries
        JavaRDD<bookObj>[] simpleQuery1 = new JavaRDD[]{mushroomStream.filter(mushroomObj -> mushroomObj.getcClass().equals("e"))};
        System.out.println("Mushroom Entries where eatable: " + simpleQuery1[0].count());

        // complex queries
        long complexQuery1= mushroomStream
                .map(mushroomObj -> {
                    if(mushroomObj.getcClass().equals("e"))
                        iCount[0] += 1;
                    else if (mushroomObj.getcClass().equals("p"))
                        iCount[0] -= 1;
                    return mushroomObj;
                })
                .filter(mushroomObj -> mushroomObj.getcClass().equals("e") && iCount[0] >= 0 & mushroomObj.getCap_shape().equals("x") ||
                        mushroomObj.getcClass().equals("p") && iCount[0] < 0 & mushroomObj.getCap_shape().equals("x"))
                .count();

        if(iCount[0] >= 0)
            System.out.println("eatable mushrooms with cap shape = x: " + complexQuery1);
        else
            System.out.println("toxic mushrooms with cap shape = x: " + complexQuery1);
    }


    public Dataset<Row>[] trainTestSplit(Dataset<Row> dfMushrooms){
        // Prepare training and test data.
        return dfMushrooms.randomSplit(new double[] {0.8, 0.2},0);
    }


    public Dataset<Row> castData(List<mushroomObj> mushroomList){
        Dataset<Row> dfMushrooms = spark.createDataFrame(mushroomList, mushroomObj.class);

        return castData(dfMushrooms);
    }


    private Dataset<Row> castData(Dataset<Row> dfMushrooms){
        dfMushrooms = dfMushrooms.withColumnRenamed("cClass", "label");
        String[] columns = dfMushrooms.columns();

        // Create Indexer
        List<StringIndexer> siList = new ArrayList<>();
        for (String col: columns) {
            StringIndexer indexer = new StringIndexer()
                    .setInputCol(col)
                    .setOutputCol(col+"_index");
            siList.add(indexer);
        }

        StringIndexer[] siArray = new StringIndexer[siList.size()];
        siArray = siList.toArray(siArray);
        Pipeline pipeline = new Pipeline()
                .setStages(siArray);
        dfMushrooms = pipeline.fit(dfMushrooms).transform(dfMushrooms);

        // Drop none indexed Columns
        for (String col: columns) {
            dfMushrooms = dfMushrooms.drop(col);
        }

        // Rename Column
        dfMushrooms = dfMushrooms.withColumnRenamed("label_index", "indexedLabel");
        columns     = dfMushrooms.columns();

        // Rename Column
        for (String col: columns) {
            if (!col.equals("indexedLabel"))
                dfMushrooms = dfMushrooms.withColumnRenamed(col, col.subSequence(0, col.length()-6).toString());
        }


        // Rearrange Columns
        dfMushrooms = dfMushrooms.select(
                "cap_shape", "cap_surface", "cap_color", "bruises", "odor",
                "gill_attachment", "gill_spacing", "gill_size", "gill_color",
                "stalk_shape", "stalk_root", "stalk_surface_above_ring",
                "stalk_surface_below_ring", "stalk_color_above_ring",
                "stalk_color_below_ring", "veil_type", "veil_color", "ring_number",
                "ring_type", "spore_print_color", "population", "habitat", "indexedLabel");

        // Label Column
        String column = "indexedLabel";

        // Feature Columns - Pre
        columns = dfMushrooms.columns();
        columns = (String[]) ArrayUtils.remove(columns, ArrayUtils.indexOf(columns, column));

        // DataFrames formatter
        VectorAssembler assembler = new VectorAssembler()
                .setInputCols(columns)
                .setOutputCol("indexedFeatures");

        dfMushrooms = assembler.transform(dfMushrooms);

        return dfMushrooms.select("indexedLabel", "indexedFeatures");
    }
}
