import Books.bookObj;
import Books.bookStream;
import Books.bookSparkPreparation;
import Mushroom.mushroomObj;
import Mushroom.mushroomStream;
import Mushroom.mushroomSparkPreparation;
import PhishingWebsites.phishingwebObj;
import PhishingWebsites.phishingwebStream;
import PhishingWebsites.phishingwebSparkPreparation;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import Receiver.JavaCustomReceiver;
import SparkML.AlgorithmOptimizer;
import SparkML.Clustering;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;


public class Main {
    public static void main(String[] args){
        Logger logger = Logger.getLogger("PROJECT");
        logger.setLevel(Level.OFF);

        // Spark
        SparkExample sparkExample = new SparkExample(logger);

        // Datasets
        Holder<Dataset<Row>> holderBooks        = new Holder<>(sparkExample.getEmptyDatasetRow());
        Holder<Dataset<Row>> holderPhishing     = new Holder<>(sparkExample.getEmptyDatasetRow());
        Holder<Dataset<Row>> holderMushrooms    = new Holder<>(sparkExample.getEmptyDatasetRow());


        // Java Stream
        doJavaStream(logger);


        // Spark
        doSparkStream(logger, holderBooks, holderPhishing, holderMushrooms);


        // Doing the ML Part
        doML(logger, holderBooks, holderPhishing, holderMushrooms);


        // Spark Streaming - Testing
        // Auskommentiert war nur zum Testen
        //testStreamFromFile();
    }


    public static class Holder<T> {
        private T value;
        public Holder(T value) {
            this.value = value;
        }
        public T getValue() {
            return value;
        }
        public void setValue(T value) {
            this.value = value;
        }
    }


    public static void doJavaStream(Logger logger){
        // Declaration - Books              [Clustering]
        bookStream bookStreamReader = new bookStream(logger);
        bookStreamReader.extract();

        // Declaration - Phishing Websites  [Classification]
        phishingwebStream phishingwebStreamReader = new phishingwebStream(logger);
        phishingwebStreamReader.extract();

        // Declaration - Mushroom           [Classification]
        mushroomStream mushroomStreamReader = new mushroomStream(logger);
        mushroomStreamReader.extract();
    }


    public static void doSparkStream(Logger logger, Holder<Dataset<Row>> holderBooks,
                                     Holder<Dataset<Row>> holderPhishing, Holder<Dataset<Row>> holderMushrooms){
        // Spark Stream Books
        bookSparkPreparation sparkBook = new bookSparkPreparation(logger);
        holderBooks.setValue(sparkBook.getData());

        // Spark Stream Phishing
        phishingwebSparkPreparation sparkPhishing = new phishingwebSparkPreparation(logger);
        holderPhishing.setValue(sparkPhishing.getData());

        // Spark Stream Mushrooms
        mushroomSparkPreparation sparkMushroom = new mushroomSparkPreparation(logger);
        holderMushrooms.setValue(sparkMushroom.getData());
    }


    public static void doML(Logger logger, Holder<Dataset<Row>> holderBooks,
                            Holder<Dataset<Row>> holderPhishing, Holder<Dataset<Row>> holderMushrooms){
        // Books    - Clustering
        try {
            Clustering clusterAlgos         = new Clustering(logger, "books");
            bookSparkPreparation bookPrep   = new bookSparkPreparation(logger);

            // Find a good k Param
            int bestKkMeans = 2;
            int bestKGMM    = 2;
            // Auskommentiert -> Dauert zu lange, kam bei beiden 2 raus
            //bestKkMeans   = clusterAlgos.optimizeKforKMeans(holderBooks.getValue());
            //bestKGMM      = clusterAlgos.optimizeKforGMM(holderBooks.getValue());

            // Show Datasetname
            System.out.println("Book Dataset");

            // Show Silhouette Score
            System.out.printf("Best Silhouette Score for kMeans is: %f\n",
                    clusterAlgos.silhouetteScoreKMeans(holderBooks.getValue(), bestKkMeans));
            System.out.printf("Best k Param for kMeans is: %d\n", bestKkMeans);
            System.out.printf("Best Silhouette Score for GMM is: %f\n",
                    clusterAlgos.silhouetteScoreGMM(holderBooks.getValue(), bestKGMM));
            System.out.printf("Best k Param for GMM is: %d\n", bestKGMM);
            System.out.println("\n\n");
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }



        // Phishing Websites - Classification
        try {
            AlgorithmOptimizer algoOptimizer            = new AlgorithmOptimizer(logger, "phishing");
            phishingwebSparkPreparation phishingPrep    = new phishingwebSparkPreparation(logger);

            // Show Dataset Name
            System.out.println("Phishing Websites Dataset");

            Dataset<Row>[] splits       = phishingPrep.trainTestSplit(holderPhishing.getValue());
            Dataset<Row> training       = splits[0];
            Dataset<Row> test           = splits[1];

            // Auskommentiert, weil bereits trainiert wurde und die Modele lokal gespeichert sind
            //algoOptimizer.fit(training);
            algoOptimizer.validate(test);
            System.out.println("\n\n");
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }


        // Mushroom - Classification
        try {
            AlgorithmOptimizer algoOptimizer         = new AlgorithmOptimizer(logger, "mushrooms");
            mushroomSparkPreparation mushroomPrep   = new mushroomSparkPreparation(logger);

            // Show Dataset Name
            System.out.println("Mushroom Dataset");

            Dataset<Row>[] splits       = mushroomPrep.trainTestSplit(holderMushrooms.getValue());
            Dataset<Row> training       = splits[0];
            Dataset<Row> test           = splits[1];

            // Auskommentiert, weil bereits trainiert wurde und die Modele lokal gespeichert sind
            //algoOptimizer.fit(training);
            algoOptimizer.validate(test);
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }
    }


    public static void testStreamFromFile(){
        String separator = "|";

        SparkConf conf = new SparkConf()
                .setAppName("SparkMushroomPreparation")
                .setMaster("local[*]")
                .set("spark.executor.memory", "16g")
                .setSparkHome(System.getenv("SPARK_HOME"));
        try {
            JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(1));
            JavaDStream<String> stream = jssc.receiverStream(new JavaCustomReceiver("../Datasets/books.csv"));

            stream.map(line -> line)
                    .print();

            jssc.start();
            jssc.awaitTermination();
        }
        catch (Exception ex){
            int i = 0;
        }
    }
}