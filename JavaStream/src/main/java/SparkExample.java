import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class SparkExample {
    // Global Variable
    java.util.logging.Logger logger;
    transient SparkSession spark;
    transient JavaSparkContext jsc;

    public SparkExample(java.util.logging.Logger logger){
        // Init
        this.logger = logger;

        // Try to Create Spark Context & Set Spark Logger
        try {
            SparkConf conf = new SparkConf()
                    .setAppName("SparkInit")
                    .setMaster("local[*]")
                    .set("spark.executor.memory", "16g")
                    .setSparkHome(System.getenv("SPARK_HOME"));

            spark = SparkSession
                    .builder()
                    .config(conf)
                    .getOrCreate();
            jsc = new JavaSparkContext(spark.sparkContext());

            Logger.getRootLogger().setLevel(Level.ERROR);
            Logger.getLogger("org").setLevel(Level.ERROR);
            Logger.getLogger("akka").setLevel(Level.ERROR);
            spark.sparkContext().setLogLevel("ERROR");
            jsc.sc().setLogLevel("ERROR");
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }
    }

    public Dataset<Row> getEmptyDatasetRow(){
        return spark.emptyDataFrame();
    }
}
