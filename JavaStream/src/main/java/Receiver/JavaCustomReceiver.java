package Receiver;

import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;


import java.io.BufferedReader;
import java.io.FileReader;
import java.net.ConnectException;

public class JavaCustomReceiver extends Receiver<String> {
    String fPath;

    public JavaCustomReceiver(String fPath) {
        super(StorageLevel.MEMORY_AND_DISK_2());
        this.fPath = fPath;
    }

    @Override
    public void onStart() {
        new Thread(this::receive).start();
    }

    @Override
    public void onStop() { }

    private void receive() {
        String userInput;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fPath));

            while (!isStopped() && (userInput = reader.readLine()) != null) {
                System.out.println("Received data '" + userInput + "'");
                store(userInput);
            }
            reader.close();

            restart("Trying to connect again");
        } catch(ConnectException ce) {
            restart("Could not connect", ce);
        } catch(Throwable t) {
            restart("Error receiving data", t);
        }
    }
}