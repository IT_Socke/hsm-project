package Receiver;

import Mushroom.mushroomObj;
import Mushroom.mushroomSparkPreparation;
import SparkML.AlgorithmOptimizer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;

import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Receiver {
    // Declaration
    static final Duration WINDOW_SIZE       = Durations.seconds(30);
    static final Duration SLIDING_INTERVAL  = Durations.seconds(10);
    static final String KAFKA_TOPIC         = "sparkstreaming";
    static final String KAFKA_SERVER        = "localhost:9092";
    static final String KAFKA_CHECKPOINT    = "D:\\Programming\\kafka_2.13-2.7.0\\checkpoint";


    /*
    https://kafka.apache.org/quickstart#
    https://dzone.com/articles/running-apache-kafka-on-windows-os

    cd /d D:\Programming\apache-zookeeper-3.6.2
    .\bin\zkServer.cmd

    cd /d D:\Programming\kafka_2.13-2.7.0
    .\bin\windows\kafka-server-start.bat .\config\server.properties
     */


    public static void main(String[] args) {
        // New Logger
        Logger logger = Logger.getLogger("PROJECT_RECEIVER");
        logger.setLevel(Level.OFF);

        // Declaration
        String separator = "|";

        //Set log level to warn
        Logger.getLogger("org").setLevel(Level.OFF);

        // Spark Settings
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("KafkaReceiver")
                .setSparkHome(System.getenv("SPARK_HOME"));
        JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(10));
        jssc.sparkContext().setLogLevel("ERROR");
        jssc.checkpoint(KAFKA_CHECKPOINT);

        // Kafka Settings
        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", KAFKA_SERVER);
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "0");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit", false);

        // Topics to Collect
        Collection<String> topics = Collections.singletonList(KAFKA_TOPIC);

        // Kafka Input Stream
        JavaInputDStream<ConsumerRecord<String, String>> stream = KafkaUtils.createDirectStream(jssc,
                LocationStrategies.PreferConsistent(), ConsumerStrategies.Subscribe(topics, kafkaParams));

        // Read from Kafka Stream
        // Alternative zur Prediction weiter unten
        /*JavaDStream<String> lines = stream
                .map((Function<ConsumerRecord<String, String>, String>) ConsumerRecord::value)
                .filter(line -> !line.equals(""))
                .window(WINDOW_SIZE, SLIDING_INTERVAL);

        // Print Income
        lines.map(line -> line)
                .print();
        */

        // Read from Kafka Stream
        JavaDStream<mushroomObj> mushroomStream = stream
                .map((Function<ConsumerRecord<String, String>, String>) ConsumerRecord::value)
                .filter(line -> !line.equals(""))
                .window(WINDOW_SIZE, SLIDING_INTERVAL)
                .map(line -> {
                    line = line.replace(",", "|");
                    try {
                        String[] attributes = line.split(Pattern.quote(separator));
                        return new mushroomObj(attributes);
                    } catch (Exception ex) {
                        return new mushroomObj();
                    }
                })
                .filter(mushroomObj -> !mushroomObj.getcClass().equals("#"));

        // Algo Optimizer Init
        AlgorithmOptimizer algoOptimizer        = new AlgorithmOptimizer("mushrooms");
        mushroomSparkPreparation sparkMushroom  = new mushroomSparkPreparation();
        algoOptimizer.loadTestAlgorithm();

        // Do Prediction from Stream
        mushroomStream.foreachRDD(mushroomObjJavaRDD -> {
            Dataset<Row> dataToPredict = sparkMushroom.castData(mushroomObjJavaRDD.collect());
            algoOptimizer.predict(dataToPredict);
        });


        jssc.start();
        try {
            jssc.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
