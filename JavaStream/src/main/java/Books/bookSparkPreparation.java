package Books;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.sql.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class bookSparkPreparation {
    // Global Variable
    java.util.logging.Logger logger;
    transient SparkSession spark;
    transient JavaSparkContext jsc;


    public bookSparkPreparation(java.util.logging.Logger logger){
        // Init
        this.logger = logger;

        // Try to Create Spark Context & Set Spark Logger
        try {
            SparkConf conf = new SparkConf()
                    .setAppName("SparkBookPreparation")
                    .setMaster("local[*]")
                    .set("spark.executor.memory", "16g")
                    .setSparkHome(System.getenv("SPARK_HOME"));

            spark = SparkSession
                    .builder()
                    .config(conf)
                    .getOrCreate();
            jsc = new JavaSparkContext(spark.sparkContext());

            Logger.getRootLogger().setLevel(Level.ERROR);
            Logger.getLogger("org").setLevel(Level.ERROR);
            Logger.getLogger("akka").setLevel(Level.ERROR);
            spark.sparkContext().setLogLevel("ERROR");
            jsc.sc().setLogLevel("ERROR");
        }
        catch (Exception ex) {
            logger.log(java.util.logging.Level.WARNING, ex.toString());
        }
    }


    public Dataset<Row> getData(){
        // Declaration
        String separator = "|";

        // Read CSV
        JavaRDD<String> data = jsc.textFile("Datasets/books.csv");

        // Cast String RDD to specific Object RDD
        JavaRDD<bookObj> rddData = data
                .map(line -> {
            line = line.replace(", ", "||")
                    .replace(",","|")
                    .replace("||", ",");
            try {
                String[] attributes = line.split(Pattern.quote(separator));
                return new bookObj(attributes);
            } catch (Exception ex) {
                return new bookObj();
            }
        })
                .filter(bookObj -> bookObj.getBookID() > -1);

        // Queries
        doQueries(rddData);

        // Create Dataframe from JavaRDD
        Dataset<Row> dfBooks = spark.createDataFrame(rddData, bookObj.class);

        // Cast List to SparkDataset
        return castData(dfBooks);
    }


    private void doQueries(JavaRDD<bookObj> bookStream) {
        // Declaration
        final int[] iMean1 = {0};
        final int[] iMean2 = {0};
        final int[] iCount = {0};

        // Simple Queries
        JavaRDD<bookObj>[] simpleQuery1 = new JavaRDD[]{bookStream.filter(bookObj -> bookObj.getTitle().length() > 10)};
        System.out.println("Books Entries where Book Title length > 10: " + simpleQuery1[0].count());

        // complex queries
        long complexQuery1 = bookStream
                .map(bookObj -> {
                    iMean1[0] += bookObj.getTitle().length();
                    iCount[0] += (1);
                    return bookObj;
                })
                .filter(bookObj -> bookObj.getTitle().length() < (iMean1[0]/iCount[0]))
                .count();
        System.out.println("Books Entries Title length < Mean Value: " + complexQuery1);
        long complexQuery2 = bookStream
                .map(bookObj -> {
                    iMean1[0] += bookObj.getAverage_rating();
                    iCount[0] += 1;
                    return bookObj;
                })
                .filter(bookObj -> bookObj.getAverage_rating() > (double)(iMean1[0]/iCount[0]))
                .count();
        System.out.println("Books Entries average_rating > Mean Value: " + complexQuery2);
        long complexQuery3 = bookStream
                .map(bookObj -> {
                    iMean1[0] += bookObj.getAverage_rating();
                    iMean2[0] += bookObj.getRatings_count();
                    iCount[0] += 1;
                    return bookObj;
                })
                .filter(bookObj -> bookObj.getAverage_rating() > (double)(iMean1[0]/iCount[0]) &&
                        bookObj.getRatings_count() > (iMean2[0]/iCount[0]))
                .count();
        System.out.println("Books Entries average_rating > Mean Value and Ratings_count > Mean Value: " + complexQuery3);
    }


    public Dataset<Row> castData(List<bookObj> bookList){
        Dataset<Row> dfBooks = spark.createDataFrame(bookList, bookObj.class);

        return castData(dfBooks);
    }


    private Dataset<Row> castData(Dataset<Row> dfBooks){
        // Select important columns
        dfBooks = selectImportantColumns(dfBooks);

        // Important columns
        String[] importantColumns = dfBooks.columns();

        // Only String columns
        String[] columns = {"title", "authors", "isbn", "language_code", "publisher"};

        // Create Indexer
        List<StringIndexer> siList = new ArrayList<>();
        for (String col: columns) {
            if(Arrays.asList(importantColumns).contains(col)) {
                StringIndexer indexer = new StringIndexer()
                        .setInputCol(col)
                        .setOutputCol(col + "_index");
                siList.add(indexer);
            }
        }

        StringIndexer[] siArray = new StringIndexer[siList.size()];
        siArray = siList.toArray(siArray);
        Pipeline pipeline = new Pipeline()
                .setStages(siArray);
        dfBooks = pipeline.fit(dfBooks).transform(dfBooks);

        // Drop none indexed Columns
        for (String col: columns) {
            dfBooks = dfBooks.drop(col);
        }

        // Get Columns
        columns = dfBooks.columns();

        // Rename Index Columns
        for (String col: columns) {
            if (!col.equals("indexedLabel") && col.contains("index"))
                dfBooks = dfBooks.withColumnRenamed(col, col.subSequence(0, col.length() - 6).toString());
        }

        // Rearrange important Columns
        dfBooks = selectImportantColumns(dfBooks);

        // Feature Columns - Pre
        columns = dfBooks.columns();

        // DataFrames formatter
        VectorAssembler assembler = new VectorAssembler()
                .setInputCols(columns)
                .setOutputCol("indexedFeatures");

        dfBooks = assembler.transform(dfBooks);

        return dfBooks.select("indexedFeatures");
    }


    private Dataset<Row> selectImportantColumns(Dataset<Row> dfBooks){
        // Important Columns
        return dfBooks.select(
                "average_rating",
                "language_code", "num_pages", "ratings_count",
                "text_reviews_count");
        /*return dfBooks.select(
                "authors", "average_rating", "isbn",
                "isbn13", "language_code", "num_pages", "ratings_count",
                "text_reviews_count", "publication_date", "publisher");*/
    }
}
