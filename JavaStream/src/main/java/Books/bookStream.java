package Books;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class bookStream {
    Logger logger;

    public bookStream(Logger logger){
        this.logger = logger;
    }


    public void extract(){
        // Deklaration
        BufferedReader br;
        String separator = "|";

        // Try to read file
        try {
            br = new BufferedReader(new FileReader("Datasets/books.csv"));
        }
        catch (Exception ignore) {
            logger.log(Level.WARNING, "File not found");
            return;
        }

        // Convert Stream to Obj List
        Stream<bookObj> bookStream  = br.lines()
                .skip(1)
                .map(line -> {
                    line = line.replace(", ", "||").replace(",","|").replace("||", ",");
                    try {
                        String[] attributes = line.split(Pattern.quote(separator));
                        return new bookObj(attributes);
                    } catch (Exception ex){
                        logger.log(Level.WARNING, ex.toString());
                        return null;
                    }
                })
                .filter(Objects::nonNull);

        doQueries(bookStream);
    }

    private void doQueries(Stream<bookObj> bookStream) {
        // Declaration
        var ref = new Object() {
            int iMean1 = 0;
            int iMean2 = 0;
            int iCount = 0;
        };

        // Simple Queries
        /*Stream<bookObj> simpleQuery1 = bookStream.filter(bookObj -> bookObj.getRatings_count() > 50000);
        System.out.println("Books Entries where Ratings_count > 50000: " + simpleQuery1.count());
        Stream<bookObj> simpleQuery2 = bookStream.filter(bookObj -> !bookObj.getLanguage_code().equals("eng"));
        System.out.println("Books Entries where getLanguage_code != eng: " + simpleQuery2.count());*/

        // complex queries
        /*long complexQuery1 = bookStream
                .peek(bookObj -> ref.iMean += bookObj.getNum_pages())
                .filter(bookObj -> bookObj.getNum_pages() < ref.iMean)
                .count();
        System.out.println("Books Entries Pages Count < Mean Value: " + complexQuery1);
        long complexQuery2 = bookStream
                .peek(bookObj -> ref.iMean += bookObj.getRatings_count())
                .filter(bookObj -> bookObj.getRatings_count() > ref.iMean)
                .count();
        System.out.println("Books Entries Ratings count > Mean Value: " + complexQuery2);*/
        long complexQuery3 = bookStream
                .peek(bookObj -> {
                    ref.iMean1 += bookObj.getRatings_count();
                    ref.iMean2 += bookObj.getText_reviews_count();
                    ref.iCount += 1;
                })
                .filter(bookObj -> bookObj.getRatings_count() > (ref.iMean1/ref.iCount) &&
                        bookObj.getText_reviews_count() > (ref.iMean2/ref.iCount))
                .count();
        System.out.println("Books Entries Ratings count < Mean Value && reviews count > Mean Value: " + complexQuery3);
    }
}
