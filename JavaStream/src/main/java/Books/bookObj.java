package Books;

public class bookObj {
    private int bookID              = -1;
    private String title            = "";
    private String authors          = "";
    private double average_rating   = 0.0;
    private String isbn             = "";
    private long isbn13             = 0;
    private String language_code    = "";
    private int num_pages           = 0;
    private int ratings_count       = 0;
    private int text_reviews_count  = 0;
    private int publication_date    = 0;
    private String publisher        = "";

    public bookObj(){}


    public bookObj(String[] s){
        setBookID(Integer.parseInt(s[0]));
        setTitle(s[1]);
        setAuthors(s[2]);
        setAverage_rating(Double.parseDouble(s[3]));
        setIsbn(s[4]);
        setIsbn13(Long.parseLong(s[5]));
        setLanguage_code(s[6]);
        setNum_pages(Integer.parseInt(s[7]));
        setRatings_count(Integer.parseInt(s[8]));
        setText_reviews_count(Integer.parseInt(s[9]));
        setPublication_date(s[10]);
        setPublisher(s[11]);
    }

    @Override
    public String toString() {
        return getTitle();
    }

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public double getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(double average_rating) {
        this.average_rating = average_rating;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public long getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(long isbn13) {
        this.isbn13 = isbn13;
    }

    public String getLanguage_code() {
        return language_code;
    }

    public void setLanguage_code(String language_code) {
        this.language_code = language_code;
    }

    public int getNum_pages() {
        return num_pages;
    }

    public void setNum_pages(int num_pages) {
        this.num_pages = num_pages;
    }

    public int getRatings_count() {
        return ratings_count;
    }

    public void setRatings_count(int ratings_count) {
        this.ratings_count = ratings_count;
    }

    public int getText_reviews_count() {
        return text_reviews_count;
    }

    public void setText_reviews_count(int text_reviews_count) {
        this.text_reviews_count = text_reviews_count;
    }

    public int getPublication_date() {
        return publication_date;
    }

    public void setPublication_date(String publication_date) {
        String day      = publication_date.substring(0, publication_date.indexOf("/"));
        String month    = publication_date.substring(publication_date.indexOf("/")+1,
                publication_date.lastIndexOf("/"));
        String year     = publication_date.substring(publication_date.lastIndexOf("/")+1);

        if (day.length() == 1)
            day = "0" + day;
        if (month.length() == 1)
            month = "0" + month;

        try {
            this.publication_date = Integer.parseInt(year + month + day);
        }
        catch (Exception ignore){
            this.publication_date = 0;
        }

    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
